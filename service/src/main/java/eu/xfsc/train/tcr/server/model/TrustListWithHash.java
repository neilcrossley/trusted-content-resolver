package eu.xfsc.train.tcr.server.model;

import eu.europa.esig.trustedlist.jaxb.tsl.TrustStatusListType;
import eu.xfsc.train.tspa.model.trustlist.TrustServiceStatusList;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@ToString
public class TrustListWithHash {
	
	private String content;
	private Object trustList;
	private String hash;
	
	public TrustStatusListType asTrustList() {
		return (TrustStatusListType) trustList;
	}
	
	public TrustServiceStatusList asTrainTrustList() {
		return (TrustServiceStatusList) trustList;
	}
	
}
