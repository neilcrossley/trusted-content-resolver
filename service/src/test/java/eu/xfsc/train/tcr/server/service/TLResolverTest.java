package eu.xfsc.train.tcr.server.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.math.BigInteger;
import java.net.URL;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import eu.europa.esig.trustedlist.jaxb.tsl.TSPType;
import eu.europa.esig.trustedlist.jaxb.tsl.TrustStatusListType;
import eu.xfsc.train.tspa.model.trustlist.TrustServiceStatusList;


@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
public class TLResolverTest {
	
	private static final String fh_TrustList_Uri = "https://tspa.trust-scheme.de/tspa_train_domain/api/v1/scheme/federation1.test.train.trust-scheme.de";
	private static final String fh_TrustList_Hash = "QmcRE4LZ2vZxyP85AetrFV3gKxLeZ6hqAW655L8nypJFuP"; 

	@Autowired
	private TLResolver tlResolver;
	
	private void checkTrustList(TrustStatusListType trustList) {
		assertNotNull(trustList);
		assertNotNull(trustList.getSchemeInformation());
		assertEquals("http://uri.etsi.org/TrstSvc/TrustedList/TSLType/EUgeneric", trustList.getSchemeInformation().getTSLType());
		assertEquals(new BigInteger("5"), trustList.getSchemeInformation().getTSLVersionIdentifier());
		assertEquals(new BigInteger("1"), trustList.getSchemeInformation().getTSLSequenceNumber());
		assertEquals("Federation 1 Notary", trustList.getSchemeInformation().getSchemeOperatorName().getName().get(0).getValue());
		assertEquals("DE:TRAIN", trustList.getSchemeInformation().getSchemeName().getName().get(0).getValue());
		assertEquals("http://uri.etsi.org/TrstSvc/TrustedList/StatusDetn/EUappropriate", trustList.getSchemeInformation().getStatusDeterminationApproach());
		assertEquals("https://dl.gi.de/handle/20.500.12116/38702", trustList.getSchemeInformation().getSchemeInformationURI().getURI().get(0).getValue());
		assertNull(trustList.getSignature());
		assertNotNull(trustList.getTrustServiceProviderList());
		assertEquals(1, trustList.getTrustServiceProviderList().getTrustServiceProvider().size());
		TSPType tsp = trustList.getTrustServiceProviderList().getTrustServiceProvider().get(0);
		assertNotNull(tsp.getTSPInformation());
		assertEquals("Notary 1", tsp.getTSPInformation().getTSPName().getName().get(0).getValue());
		assertEquals(2, tsp.getTSPInformation().getTSPTradeName().getName().size());
		assertEquals("https://notary1.info/TRAIN/info", tsp.getTSPInformation().getTSPInformationURI().getURI().get(0).getValue());
		assertNotNull(tsp.getTSPServices());
		assertEquals(5, tsp.getTSPServices().getTSPService().size());
		assertEquals("http://uri.etsi.org/19612/TSLTag", trustList.getTSLTag());
	}

	@Test
	public void testTLResolveXMLFile() throws Exception {
		URL url = this.getClass().getResource("/SampleTrustList.xml");
		File file = new File(url.getFile());
		assertTrue(file.exists());
		TrustStatusListType list = tlResolver.resolveTL(file.getCanonicalFile().toURI().toString());
		checkTrustList(list);
	}

	@Test
	public void testTLResolveJSONFile() throws Exception {
		URL url = this.getClass().getResource("/SampleTrustList.json");
		File file = new File(url.getFile());
		assertTrue(file.exists());
		TrustStatusListType list = tlResolver.resolveTL(file.getCanonicalFile().toURI().toString());
		checkTrustList(list);
	}

	@Test
	public void testTLResolveJSONFH() throws Exception {
		URL url = this.getClass().getResource("/SampleTrustList_FH.json");
		File file = new File(url.getFile());
		assertTrue(file.exists());
		TrustServiceStatusList list = tlResolver.resolveCustomTL(file.getCanonicalFile().toURI().toString(), false);
		//checkTrustList(list);
		assertNotNull(list);
		assertNotNull(list.getFrameworkInformation());
		assertEquals("alice.trust.train1.xfsc.dev", list.getFrameworkInformation().getFrameworkName().getName());
		assertNotNull(list.getTrustServiceProviderList());
		assertNotNull(list.getTrustServiceProviderList().getTrustServiceProvider());
	}

	@Test
	public void testTLResolveCustomXMLUri() throws Exception {
		TrustServiceStatusList list = tlResolver.resolveCustomTL(fh_TrustList_Uri, false);
		assertNotNull(list);
		assertNotNull(list.getFrameworkInformation());
		assertEquals("Federation 1", list.getFrameworkInformation().getFrameworkOperatorName().getName());
		assertNotNull(list.getTrustServiceProviderList());
		assertEquals(1, list.getTrustServiceProviderList().getTrustServiceProvider().size());
	}

	@Test
	public void testTLResolveWithHash() throws Exception {
		TLResolver.TLResolveResult tlRes = tlResolver.resolveTLHash(fh_TrustList_Uri, true, fh_TrustList_Hash);
		assertNotNull(tlRes);
		assertTrue(tlRes.isHashVerified());
	}
	
}