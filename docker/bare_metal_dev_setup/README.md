Automation for provisioning OS developer environments is done through Ansible playbooks for Ubuntu and macOS.

On macOS, playbooks are executed manually from time to time on developers' machines.

On Linux, it can be integrated into CI/CD with:
```bash
$ export TRAIN_CONTAINER_ENGINE=podman # if podman is preferred against docker
$ make rmi # delete previuos image with all containters
$ make build # add to ubuntu image some requorements for make, go, pyhton, java and js 
$ make ubuntu_bash # log into ubunut
> make ansible # provison ubuntu with all tools requires for develop components in go, pyhton, java and js
```