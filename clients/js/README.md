# TCR JavaScript client


The client source code generated from TCR service OpenApi specification with help of `/openapi-genarator-maven-plugin`. To tune JS generation please see the plugin
configuration details:
- [https://github.com/OpenAPITools/openapi-generator](https://github.com/OpenAPITools/openapi-generator)
- [https://github.com/OpenAPITools/openapi-generator/blob/master/docs/generators/javascript.md](https://github.com/OpenAPITools/openapi-generator/blob/master/docs/generators/javascript.md)

The client code is generated as part of overall TCR build process, so it requires Java v21 and Maven v3.5.x (or newer).