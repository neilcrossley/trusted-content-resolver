# Trusted Content Resolver

## Description
XFSC Trusted Content Resolver Service allows Trusted Framework Pointers resolution and validation.

This is a Reference Implementation of the [Gaia-X TRAIN Lot](https://eclipse.dev/xfsc/train/train/#trusted-content-resolver).

## Documentation
All service documentation, installation instructions and other materials can be found in the [TCR documentation section](./docs).

## Support
To get support you can open an issue in the project [Issues](https://gitlab.eclipse.org/eclipse/xfsc/train/trusted-content-resolver/-/issues) section.


## Getting Started
To start with TCR project please follow the instructions: [Steps to build TCR](./docker/README.md).

## Roadmap
The project v1.0.0 will be released in Decembery 2023.

## Contributing
If you want to contribute to the project - please request a membership at [Project Members](https://gitlab.eclipse.org/eclipse/xfsc/train/trusted-content-resolver/-/project_members) section or drop an email to project maintainer [Denis Sukhoroslov](mailto:dsukhoroslov@gmail.com).

## Authors and acknowledgment
The project is implemented by T-Systems International GmbH, project members are:
- [Andrei Danciuc](https://gitlab.eclipse.org/andreidanciuc)
- [Denis Sukhoroslov](https://gitlab.eclipse.org/dsukhoroslov)
- [Frank Kassigkeit](https://gitlab.eclipse.org/fkassigk)
- [Julius Helm](https://gitlab.eclipse.org/jhtsi)
- [Martin Siegloch](https://gitlab.eclipse.org/pmsmartin)
- [Michael Zigldrum](https://gitlab.eclipse.org/mzigldrum)

## License
XFSC Trusted Content Resolver Service is Open Source software released under the [Apache 2.0 license](https://www.apache.org/licenses/LICENSE-2.0.html).
