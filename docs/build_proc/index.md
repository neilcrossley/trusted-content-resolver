## Build Procedures

Ensure you have JDK 17 (or newer), Maven 3.5.4 (or newer) and Git installed

First clone the TCR repository:

```bash
git clone --recurse-submodules https://gitlab.eclipse.org/eclipse/xfsc/train/trusted-content-resolver.git
```
Then go to the project folder and build it with maven:

```bash
mvn clean install
```

This will build all modules and run TCR testsuite.

Now we can run TCR locally. Go to `/service` folder and start it with maven:

```bash
cd service
mvn spring-boot:run
```
 
This command will start TCR service on your localhost at port `8087`, to check service status open its health page in browser at `http://localhost:8087/actuator/health`'.
 
We can also run the TCR Service with docker: go to `/docker` folder and follow instructions in the [readme](../../docker).
