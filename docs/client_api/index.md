## TCR Client API

The TCR service provides client libraries which can be used to simplify access to the TCR backend and hides details of underlying HTTP/REST communication. Client libraries are provided for the following programming languages:

- [Java](./java)
- [Go](./golang)
- [JavaScript](./javascript)
- [Python](./python)

The structures used in requests/responses are specified in the TCR [OpenAPI contract](../../openapi/tcr_openapi.yaml) and on the diagram below:  


![TCR Request/Response Model](../rest_api/images/rest_api.drawio.png "TCR Request/Response Model") 

