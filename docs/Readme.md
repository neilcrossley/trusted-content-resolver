# Trusted Content Resolver Documentation

This documentation serves as a comprehensive guide for Trusted Content Resolver (TCR) project, providing in-depth insights into its architecture, REST API, build procedures, installation & configuration and client API. Developers and administrators will find essential information to install, configure and use the software effectively.


- [Architecture](./arch)
- [REST API](./rest_api)
- [Build Procedures](./build_proc)
- [Installation & Configuration](./install)
- [Client API](./client_api)
